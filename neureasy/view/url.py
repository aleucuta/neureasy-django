"""neureasy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from view.views import index,login,register,add_user,home,login_page,register_page,profile_user,profile_trainer

urlpatterns = [
	path('start/', index, name = "start"),
    path('login/', login, name = "login"),
    path('login_page/', login_page, name = "login_page"),
    path('register/', register, name = "register"),
    path('register_page/', register_page, name = "register_page"),
    path('add_user/',add_user, name = 'add_user'),
    path('home/',home, name = 'Home'),
    path('profile_user/', profile_user, name = 'profile_user'),
    path('profile_trainer/', profile_trainer, name = 'profile_trainer')
    
]   
