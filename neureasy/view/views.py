from django.shortcuts import render
from django.db import IntegrityError
from django.http import HttpResponse
from controller.models import UserFactory,NormalUser,TrainerUser
from datetime import date
from hashlib import md5
import re
import string
import random

emailRegex    = re.compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
passwordRegex = re.compile(r"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$")
trainer = TrainerUser()
def index(request):
	return render(request, "index.html")

def profile_user(request):
	return render(request, "profile.html")

def profile_trainer(request):
	return render(request, "profile_t.html")


def login(request):
	print("starting login")
	username = request.POST.get('username')
	password = request.POST.get('password')
	try:
		user = NormalUser.objects.get(username = username)
		passMD5 = md5(password.encode()).hexdigest()
		if(user.password == passMD5):
			#return HttpResponse("<h1> Logging in as " + str(username)+ " normal user</h1>")
			return profile_user(request)
			

	except NormalUser.DoesNotExist:
		user = TrainerUser.objects.get(username = username)
		passMD5 = md5(password.encode()).hexdigest()
		if(user.password == passMD5):
			#return HttpResponse("<h1> Logging in as " + str(username)+ " trainer user</h1>")
			return profile_trainer(request)
			
	except:
		return HttpResponse("<h1> Invalid username or password </h1>") 
	
def register(request):
	return render(request, "registerOld.html")

def home(request):
	return render(request, "home.html")

def login_page(request):
	return render(request, "login.html")

def register_page(request):
	return render(request, "register.html")

def add_user(request):
	if "dummyTest" in request.POST:
		return add_user_dummy()
	username = request.POST.get('username')
	password = request.POST.get('password')
	email    = request.POST.get('email')
	joinDate = date.today()
	if(emailRegex.match(email) == False):
		return HttpResponse("<h1> Invalid email!</h1>")
	if(len(username) > 100):
		return HttpResponse("<h1> Username too large!</h1>")
	if(len(username) < 4):
		return HttpResponse("<h1> Username too small! Type at least 4 characters you lazy piece of shit! </h1>")
	if(len(password) < 8 or len(password) > 100 or passwordRegex.match(password) == False):
		return HttpResponse("<h1> Invalid password! Password needs to be at least 8 characters, have a number (0 - 9), uppercase letters, lowercase letters and any special character (#@$!~ 'you' )!</h1>")		
	
	singularity = checkSingularity(username,email)
	if(singularity == None):
		if "clientButton" in request.POST:
			return add_user_client(username,password,email,joinDate)
		if "trainerButton" in request.POST:
			return add_user_trainer(username,password,email,joinDate)
	else:
		return singularity

def checkSingularity(username,email):
	#singularity checking
	ok = 0
	requiredOk = 1 | 2 | 4 | 8
	try:
		NormalUser.objects.get(username = username)
		#if there is a user with this username
		return HttpResponse("<h1> A user with this username already exists!<h1>")
	except NormalUser.DoesNotExist:
		ok|=1
	try:
		NormalUser.objects.get(email = email)
		return HttpResponse("<h1> A user with this email already exists!<h1>")
	except NormalUser.DoesNotExist:
		ok|=2
	try:
		TrainerUser.objects.get(username = username)
		return HttpResponse("<h1> A user with this username already exists!<h1>")
	except TrainerUser.DoesNotExist:
		ok|=4
	try:
		TrainerUser.objects.get(email = email)
		return HttpResponse("<h1> A user with this email already exists!<h1>")
	except TrainerUser.DoesNotExist:
		ok|=8		
	return None
	

def add_user_client(username,password,email,joinDate):
	password = md5(password.encode()).hexdigest()
	myUser = UserFactory()
	new_entry = myUser.createUser("NormalUser",{
						"username" 	: username,
						"password" 	: password,
						"email"		: email,
						"joinDate"	: joinDate})
	new_entry.save()
	return HttpResponse("<h1> Normal User added successfully!</h1>")

def add_user_trainer(username,password,email,joinDate):
	password = md5(password.encode()).hexdigest()
	factory = UserFactory()
	new_entry = factory.createUser("TrainerUser",{
						"username" 	: username,
						"password" 	: password,
						"email"		: email,
						"joinDate"	: joinDate})
	new_entry.save()
	return HttpResponse("<h1> Trainer User added successfully!</h1>")

def add_user_dummy():
	letters = string.ascii_lowercase
	username = ""
	password = ""
	email = ""
	for i in range(10):
		username = username+random.choice(letters)
		password = password+random.choice(letters)
		email = email+random.choice(letters)
	email=email+"@yahoo.com"
	password_md5 = md5(password.encode()).hexdigest()
	factory = UserFactory()
	new_entry = factory.createUser("NormalUser",{
							"username" 	: username,
							"password" 	: password_md5,
							"email"		: email,
							"joinDate"	: date.today()})
	new_entry.save()
	return HttpResponse("<h1> Dummy user added successfully</h1>")