from __future__ import absolute_import, division, print_function, unicode_literals, annotations

import tensorflow as tf
import tensorflow.keras.layers
from tensorflow.keras.layers import Dense, Flatten, Conv2D
from tensorflow.keras import Model
from tensorflow.keras.models import clone_model
from copy import deepcopy
from abc import ABC, abstractmethod, abstractproperty
from typing import Any
import numpy as np
from nn_model import *

class TrainingModel():
    def __init__(self, data_set=None, data_set_path=None, ownerId="42", \
                rewardPoints=1.0, maxEpoch=5, minAccuracy=99.0, model=None, \
                    nn_args=None, model_weights_file="nn_model_weights.h5"):
        self.ownerId = ownerId
        self.reward_points = rewardPoints
        self.max_epoch = maxEpoch
        self.min_accuracy = minAccuracy
        self.model = model
        self.nn_args = nn_args
        self.data_set = data_set #(train_ds, test_ds)
        self.data_set_path = data_set_path
        self.model_weights_file = model_weights_file
        #pentru constructia de NnModel
        self.director = NNModelDirector()
        self.builder = ConcreteModelBuilder()
        self.director.builder = self.builder
        #pentru metrica
        self.train_loss = tf.keras.metrics.Mean(name='train_loss')
        self.train_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='train_accuracy')
        self.test_loss = tf.keras.metrics.Mean(name='test_loss')
        self.test_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='test_accuracy')
        self.current_epoch = 0

    def save_weights(self) -> None:
        self.model.save_weights(self.model_weights_file, save_format='h5')

    def load_weights(self) -> None:
        if self.current_epoch == 0 or self.model == None:
            if self.model == None:
                self.model = self.create_model()
            self.init_model()
        
        self.model.load_weights(self.model_weights_file)
            

    def create_model(self) -> NnModel:
        self.director.reset_builder_product()
        self.director.build_full_feature_product(self.nn_args)
        return self.director.get_builder_product()

    def init_model(self) -> NnModel:
        images, labels = next(iter(self.data_set["train_ds"]))
        self.model.train_step(images, labels)
        # self.train_model()

    def train_model(self):
        for images, labels in self.data_set["train_ds"]:
            (loss, labels, predictions) = self.model.train_step(images, labels)
            self.train_loss(loss)
            self.train_accuracy(labels, predictions)
    
    def test_model(self):
        for test_images, test_labels in self.data_set["test_ds"]:
            (t_loss, labels, predictions) = self.model.test_step(test_images, test_labels)
            self.test_loss(t_loss)
            self.test_accuracy(labels, predictions) 

    def train(self, max_epoch=-1, min_accuracy=100, print_result=False):
        while   (max_epoch > self.current_epoch or max_epoch == -1) and (min_accuracy > (self.test_accuracy.result()*100)):
            self.current_epoch += 1
            self.reset_meters_states()
            self.train_model()
            self.test_model()
            if print_result:
                template = 'Epoch {}, Loss: {}, Accuracy: {}, Test Loss: {}, Test Accuracy: {}'
                print(template.format(self.current_epoch,
                                self.train_loss.result(),
                                self.train_accuracy.result()*100,
                                self.test_loss.result(),
                                self.test_accuracy.result()*100))
 
    def reset_meters_states(self) -> None:
        self.train_loss.reset_states()
        self.train_accuracy.reset_states()
        self.test_loss.reset_states()
        self.test_accuracy.reset_states()

    def load_data_file(self):
        path = self.data_set_path
        with np.load(path) as f:
            x_train, y_train = f['x_train'], f['y_train']
            
            x_test, y_test = f['x_test'], f['y_test']

        x_train, x_test = x_train / 255.0, x_test / 255.0
        
        x_train = x_train[..., tf.newaxis]
        x_test = x_test[..., tf.newaxis]
        
        
        train_ds = tf.data.Dataset.from_tensor_slices(
        (x_train, y_train)).shuffle(10000).batch(32)
        test_ds = tf.data.Dataset.from_tensor_slices((x_test, y_test)).batch(32)


        data_set = {
            "train_ds" : train_ds,
            "test_ds" : test_ds
        }
        
        self.data_set = data_set