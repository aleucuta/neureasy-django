from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf
import tensorflow.keras.layers
from tensorflow.keras.layers import Dense, Flatten, Conv2D
from tensorflow.keras import Model
from tensorflow.keras.models import clone_model
from copy import deepcopy
import json
import test_nn
from nn_model import *
from training_model import *
import numpy  as np
import os


nn_args = { 
    "layers" :
    [
        {
            "layer" : "Conv2D",
            "args" : {
                        "filters" : 32,
                        "kernel_size" : 3,
                        "activation" : "relu"
                    }
        },
        {
            "layer" : "Flatten",
            "args" : {
                    }
        },
        {
            "layer" : "Dense",
            "args" : {
                        "units" : 128,
                        "activation" : "relu"
                    }
        },
        {
            "layer" : "Dense",
            "args" : {
                        "units" : 10,
                        "activation" : "softmax"
                    }
        }
    ],
    "loss_object" : "SparseCategoricalCrossentropy",
    "optimizer" : "Adam"
}

def test_data_set():
   
    path = "data.file"
    mnist = tf.keras.datasets.mnist
    mnist.load_data(os.getcwd() + "\\" + path)
    print(mnist)
    with np.load(path) as f:
        
        # try:
        #     json.dump(f, open("ds.json"))
        # except:
        #     print("Nu a mers")
        #     pass
        x_train, y_train = f['x_train'], f['y_train']
        
        x_test, y_test = f['x_test'], f['y_test']


    x_train, x_test = x_train / 255.0, x_test / 255.0
    # Add a channels dimension
    x_train = x_train[..., tf.newaxis]
    x_test = x_test[..., tf.newaxis]
    
    # Batch and shuffle the data set
    train_ds = tf.data.Dataset.from_tensor_slices(
    (x_train, y_train)).shuffle(10000).batch(32)
    test_ds = tf.data.Dataset.from_tensor_slices((x_test, y_test)).batch(32)


    data_set = {
        "train_ds" : train_ds,
        "test_ds" : test_ds
    }
    
    return data_set

def test_load_data():
    model = TrainingModel(data_set_path="data.file",nn_args=nn_args)
    model.load_data_file()
    model.model = model.create_model() 
    print("\n[SUCCESS] Nu a crapat la constructie\n")
    return model

def test_construction(data_set=None):
    model = TrainingModel(data_set=data_set,nn_args=nn_args)
    model.model = model.create_model() 
    print("\n[SUCCESS] Nu a crapat la constructie\n")
    return model

def test_train(t_model=None):
    t_model.train(max_epoch=3, min_accuracy=99.5, print_result=True)

    print("\n[SUCCESS] Nu a crapat la antrenare\n")
    return t_model


def test_load(t_model=None):
    #demonstrare salvare greutati - se salveaza doar greutatile, nu toata stare
    t_model.train(max_epoch=1, min_accuracy=99.5, print_result=True)
    t_model.save_weights()
    t_model2 = test_construction(t_model.data_set)
    t_model2.current_epoch = 0

    print(t_model2.model.layers)
    print("\n[INFO] S-au salvat greutatile\n")
    t_model.train(max_epoch=2, min_accuracy=99.5, print_result=True)
    
    
    
    t_model2.load_weights()
    t_model2.current_epoch = 1
    print("\n[INFO] S-au incarcat greutatile\n")
    t_model2.train(max_epoch=2, min_accuracy=99.5, print_result=True)
    

    print("\n[SUCCESS] Nu a crapat la antrenare\n")
    return t_model

if __name__ == "__main__":
    tf.keras.backend.set_floatx('float64')
    # data_set = test_data_set()
    # t_model = test_construction(data_set)
    # # t_model.init_model()
    # # # print(t_model.model.layers)
    # # # print(test_nn.test_construction().layers)
    # # # test_nn.test_functionatity(t_model.model, test_nn.test_data_set())
    # # # test_train(t_model)
    # test_load(t_model)

    t_model = test_load_data()
    test_load(t_model)