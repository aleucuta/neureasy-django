from django.db import models

def makeUser(userType,kwargs):
	if userType == "NormalUser":
		return NormalUser().create(kwargs)
	if userType == "TrainerUser":
		return TrainerUser().create(kwargs)
	#if not any
	raise Exception("Bad user type. Use \"NormalUser\" or \"Trainer User\"")


class UserFactory(models.Model):
	def createUser(self,userType,kwargs):
		print ("userType",userType,"\nkwargs",kwargs)
		if 	"username"	not in kwargs or\
			"password"	not in kwargs or\
			"email"		not in kwargs or\
			"joinDate"	not in kwargs:
			raise Exception("Bad user details")
		else:
			user = makeUser(userType,kwargs)
			return user 


class NormalUser(models.Model):
	username = models.CharField(max_length = 50)
	password = models.CharField(max_length = 33)
	email    = models.CharField(max_length = 50)
	joinDate = models.DateField()

	def create(self,kwargs):
		self.username 	= kwargs["username"]
		self.password 	= kwargs["password"]
		self.email 		= kwargs["email"]
		self.joinDate 	= kwargs["joinDate"]
		return self



class TrainerUser(models.Model):
	username 	= models.CharField(max_length = 50)
	password 	= models.CharField(max_length = 33)
	email    	= models.CharField(max_length = 50)
	joinDate 	= models.DateField()
	listening 	= models.BooleanField()

	def create(self,kwargs):
		self.username 	= kwargs["username"]
		self.password 	= kwargs["password"]
		self.email 		= kwargs["email"]
		self.joinDate 	= kwargs["joinDate"]
		self.listening 	= False
		return self

	def startListening(self):
		self.listening = True
		return
 