from __future__ import absolute_import, division, print_function, unicode_literals, annotations

import tensorflow as tf
import tensorflow.keras.layers
from tensorflow.keras.layers import Dense, Flatten, Conv2D
from tensorflow.keras import Model
from tensorflow.keras.models import clone_model
from copy import deepcopy
from abc import ABC, abstractmethod, abstractproperty
from typing import Any





class Prototype(ABC):

    @abstractmethod
    def clone(self) -> Prototype:
        pass 

class AbstractBuilder(ABC):

    @abstractproperty
    def product(self) -> None:
        pass

    @abstractmethod
    def produce_layer(self) -> None:
        pass

    @abstractmethod
    def reset(self) -> None:
        pass

    @abstractmethod
    def produce_loss_object(self) -> None:
        pass

    @abstractmethod
    def produce_optimizer(self) -> None:
        pass

class ConcreteModelBuilder(AbstractBuilder):

    def __init__(self) -> None:
        self.reset()

    def reset(self) -> None:
        self._product = NnModel()

    @property
    def product(self) -> NnModel:
        product = self._product
        self.reset()
        return product

    def produce_layer(self, layer) -> None:
        self._product.add_layer(layer)

    def produce_loss_object(self, loss_object) -> None:
        self._product.add_loss_object(loss_object)

    def produce_optimizer(self, optimizer) -> None:
        self._product.add_optimizer(optimizer)


class NNModelDirector:
    def __init__(self) -> None:
        self._builder = None

    @property
    def builder(self) -> AbstractBuilder:
        return self._builder

    @builder.setter
    def builder(self, builder: AbstractBuilder) -> None:
        self._builder = builder

    def build_full_feature_product(self, model_arguments):
        for layer in model_arguments["layers"]:
            self._builder.produce_layer(layer)
        
        self._builder.produce_loss_object(model_arguments["loss_object"])
        self._builder.produce_optimizer(model_arguments["optimizer"])
        
    def reset_builder_product(self) -> None:
        self._builder.reset()
        
    def get_builder_product(self) -> NnModel:
        return self._builder.product





class NnModel(Model, Prototype):
    def __init__(self):
        super(NnModel, self).__init__()
        self.nn_layers = []
        self.loss_object = None
        self.optimizer = None
        
       
    def add_layer(self, layer):
     
        new_layer = getattr(tensorflow.keras.layers, layer["layer"])(**layer["args"])
        self.nn_layers.append(new_layer)

    def add_loss_object(self, loss_object):
        new_loss_object = getattr(tf.keras.losses, loss_object)()
        self.loss_object = new_loss_object

    def add_optimizer(self, optimizer):
        new_optimizer = getattr(tf.keras.optimizers, optimizer)()
        self.optimizer = new_optimizer

    def call(self, x):
        for layer in self.nn_layers:
            x = layer(x)
        return x

    def clone(self):
        new_clone = NnModel()
        
        new_clone.loss_object = self.loss_object
        new_clone.optimizer = self.optimizer
        new_clone._set_inputs(inputs=self.inputs, outputs=self.outputs)
        new_clone.set_weights(self.get_weights())
        return new_clone
            
    @tf.function
    def train_step(self, images, labels):
        with tf.GradientTape() as tape:
            predictions = self(images)
            loss = self.loss_object(labels, predictions)
        gradients = tape.gradient(loss, self.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, self.trainable_variables))

        # train_loss(loss)
        # train_accuracy(labels, predictions)
        return (loss, labels, predictions)

    @tf.function
    def test_step(self, images, labels):
        predictions = self(images)
        t_loss = self.loss_object(labels, predictions)

        # test_loss(t_loss)
        # test_accuracy(labels, predictions)

        return (t_loss, labels, predictions)

    def save_to_file(self, path="model.h5") -> None:
        self.save(path)

    def load_from_file(self, path="model.h5") -> NnModel:
        return tf.keras.models.load_model(path)


