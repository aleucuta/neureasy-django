from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf
import tensorflow.keras.layers
from tensorflow.keras.layers import Dense, Flatten, Conv2D
from tensorflow.keras import Model
from tensorflow.keras.models import clone_model
from copy import deepcopy
import json
from nn_model import *
import numpy as np

nn_args = { 
    "layers" :
    [
        {
            "layer" : "Conv2D",
            "args" : {
                        "filters" : 32,
                        "kernel_size" : 3,
                        "activation" : "relu"
                    }
        },
        {
            "layer" : "Flatten",
            "args" : {
                    }
        },
        {
            "layer" : "Dense",
            "args" : {
                        "units" : 128,
                        "activation" : "relu"
                    }
        },
        {
            "layer" : "Dense",
            "args" : {
                        "units" : 10,
                        "activation" : "softmax"
                    }
        }
    ],
    "loss_object" : "SparseCategoricalCrossentropy",
    "optimizer" : "Adam"
}

def test_data_set():
   
    path = "D:\\Facultate\\an3\\is\\neureasy\\neureasy\\controller\\data.file"
    mnist = tf.keras.datasets.mnist
    print(mnist)
    with np.load(path) as f:
        x_train, y_train = f['x_train'], f['y_train']
        x_test, y_test = f['x_test'], f['y_test']


    x_train, x_test = x_train / 255.0, x_test / 255.0
    # Add a channels dimension
    x_train = x_train[..., tf.newaxis]
    x_test = x_test[..., tf.newaxis]

    # Batch and shuffle the data set
    train_ds = tf.data.Dataset.from_tensor_slices(
    (x_train, y_train)).shuffle(10000).batch(32)
    test_ds = tf.data.Dataset.from_tensor_slices((x_test, y_test)).batch(32)

    return (train_ds, test_ds)

def test_functionatity(model, data_set):
    # Initializing the model
    #model = NnModel()
    train_ds, test_ds = data_set
    path = "D:\\Facultate\\an3\\is\\neureasy\\neureasy\\controller\\data.file"
    mnist = tf.keras.datasets.mnist
    print(mnist)
    with np.load(path) as f:
        x_train, y_train = f['x_train'], f['y_train']
        x_test, y_test = f['x_test'], f['y_test']

    

    # Metrics to measure loss and accuracy of the model
    train_loss = tf.keras.metrics.Mean(name='train_loss')
    train_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='train_accuracy')

    test_loss = tf.keras.metrics.Mean(name='test_loss')
    test_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='test_accuracy')

    
    EPOCHS = 1
    
    for epoch in range(EPOCHS):
        for images, labels in train_ds:
            (loss, tabels, predictions) = model.train_step(images, labels)
            train_loss(loss)
            train_accuracy(labels, predictions)

        for test_images, test_labels in test_ds:
            (t_loss, labels, predictions) = model.test_step(test_images, test_labels)
            test_loss(t_loss)
            test_accuracy(labels, predictions)

        template = 'Epoch {}, Loss: {}, Accuracy: {}, Test Loss: {}, Test Accuracy: {}'
        print(template.format(epoch+1,
                                train_loss.result(),
                                train_accuracy.result()*100,
                                test_loss.result(),
                                test_accuracy.result()*100))

        # Reset the metrics for the next epoch
        train_loss.reset_states()
        train_accuracy.reset_states()
        test_loss.reset_states()
        test_accuracy.reset_states()

    # print("[DEBUG]")
    # try:
    #     print(model.inputs)
    # except:
    #     pass
    model2 = test_construction()
    model2._set_inputs(inputs=model.inputs, outputs=model.outputs)
    model2.set_weights(model.get_weights())
    # model.save_to_file()
    #testare pentru mine
    for images, labels in train_ds:
        (loss, tabels, predictions) = model.train_step(images, labels)
        train_loss(loss)
        train_accuracy(labels, predictions)

    for test_images, test_labels in test_ds:
        (t_loss, labels, predictions) = model.test_step(test_images, test_labels)
        test_loss(t_loss)
        test_accuracy(labels, predictions)

    template = 'Epoch {}, Loss: {}, Accuracy: {}, Test Loss: {}, Test Accuracy: {}'
    print(template.format(6,
                            train_loss.result(),
                            train_accuracy.result()*100,
                            test_loss.result(),
                            test_accuracy.result()*100))

    # Reset the metrics for the next epoch
    train_loss.reset_states()
    train_accuracy.reset_states()
    test_loss.reset_states()
    test_accuracy.reset_states()

  

    #testare de demonstratie
    # loss_object = tf.keras.losses.SparseCategoricalCrossentropy()
    # optimizer = tf.keras.optimizers.Adam()
    model = None
    for images, labels in train_ds:
        (loss, tabels, predictions) = model2.train_step(images, labels)
        train_loss(loss)
        train_accuracy(labels, predictions)

    for test_images, test_labels in test_ds:
        (t_loss, labels, predictions) = model2.test_step(test_images, test_labels)
        test_loss(t_loss)
        test_accuracy(labels, predictions)

    template = 'Epoch {}, Loss: {}, Accuracy: {}, Test Loss: {}, Test Accuracy: {}'
    print(template.format(-1,
                            train_loss.result(),
                            train_accuracy.result()*100,
                            test_loss.result(),
                            test_accuracy.result()*100))

    # Reset the metrics for the next epoch
    train_loss.reset_states()
    train_accuracy.reset_states()
    test_loss.reset_states()
    test_accuracy.reset_states()
    
    # model = NnModel.load_from_file()

    # for images, labels in train_ds:
    #     (loss, tabels, predictions) = model.train_step(images, labels)
    #     train_loss(loss)
    #     train_accuracy(labels, predictions)

    # for test_images, test_labels in test_ds:
    #     (t_loss, labels, predictions) = model.test_step(test_images, test_labels)
    #     test_loss(t_loss)
    #     test_accuracy(labels, predictions)

    # template = 'Epoch {}, Loss: {}, Accuracy: {}, Test Loss: {}, Test Accuracy: {}'
    # print(template.format(clona,
    #                         train_loss.result(),
    #                         train_accuracy.result()*100,
    #                         test_loss.result(),
    #                         test_accuracy.result()*100))
    print("\n[SUCCESS] Nu a crapat la rulare\n")


def test_construction() -> NnModel:
    builder = ConcreteModelBuilder()
    director = NNModelDirector()

    director.builder = builder



    director.build_full_feature_product(nn_args)

    

    model = builder.product
    print("\n[SUCCESS] Nu a crapat la constructie\n")
    return model

def test_demo():
    mnist = tf.keras.datasets.mnist

    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x_train, x_test = x_train / 255.0, x_test / 255.0

    # Add a channels dimension
    x_train = x_train[..., tf.newaxis]
    x_test = x_test[..., tf.newaxis]

    train_ds = tf.data.Dataset.from_tensor_slices(
    (x_train, y_train)).shuffle(10000).batch(32)

    test_ds = tf.data.Dataset.from_tensor_slices((x_test, y_test)).batch(32)

    class MyModel(Model):
        def __init__(self):
            super(MyModel, self).__init__()
            # self.conv1 = Conv2D(32, 3, activation='relu')
            # self.flatten = Flatten()
            # self.d1 = Dense(128, activation='relu')
            # self.d2 = Dense(10, activation='softmax')
            self.nn_layers = []
            for x in nn_args["layers"]:
                self.nn_layers.append(getattr(tensorflow.keras.layers, x["layer"])(**x["args"]))
            # self.nn_layers.append(Conv2D(32, 3, activation='relu'))
            # self.nn_layers.append(Flatten())
            # self.nn_layers.append(Dense(128, activation='relu'))
            # self.nn_layers.append(Dense(10, activation='softmax'))

        def call(self, x):
            # x = self.conv1(x)
            # x = self.flatten(x)
            # x = self.d1(x)
            # return self.d2(x)
            for layer in self.nn_layers:
                x = layer(x)
            return x

    # Create an instance of the model
    model = MyModel()


    loss_object = tf.keras.losses.SparseCategoricalCrossentropy()

    optimizer = tf.keras.optimizers.Adam()

    train_loss = tf.keras.metrics.Mean(name='train_loss')
    train_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='train_accuracy')

    test_loss = tf.keras.metrics.Mean(name='test_loss')
    test_accuracy = tf.keras.metrics.SparseCategoricalAccuracy(name='test_accuracy')

    @tf.function
    def train_step(images, labels):
        with tf.GradientTape() as tape:
            predictions = model(images)
            loss = loss_object(labels, predictions)
        gradients = tape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(gradients, model.trainable_variables))

        train_loss(loss)
        train_accuracy(labels, predictions)

    @tf.function
    def test_step(images, labels):
        predictions = model(images)
        t_loss = loss_object(labels, predictions)

        test_loss(t_loss)
        test_accuracy(labels, predictions)

    EPOCHS = 5

    for epoch in range(EPOCHS):
    # Reset the metrics at the start of the next epoch
        train_loss.reset_states()
        train_accuracy.reset_states()
        test_loss.reset_states()
        test_accuracy.reset_states()

        for images, labels in train_ds:
            train_step(images, labels)

        for test_images, test_labels in test_ds:
            test_step(test_images, test_labels)

        template = 'Epoch {}, Loss: {}, Accuracy: {}, Test Loss: {}, Test Accuracy: {}'
        print(template.format(epoch+1,
                                train_loss.result(),
                                train_accuracy.result()*100,
                                test_loss.result(),
                                test_accuracy.result()*100))

if __name__ == "__main__":
    tf.keras.backend.set_floatx('float64')

    (train_ds, test_ds) = test_data_set()

    model = test_construction()

    test_functionatity(model, data_set=(train_ds, test_ds))
    
    # test_demo()